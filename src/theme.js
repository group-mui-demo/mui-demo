import { createTheme } from '@mui/material/styles'
import { green, grey, purple, yellow } from '@mui/material/colors'

export const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
      light: 'skyblue',
    },
    secondary: {
      main: yellow[500],
      light: green[500],
    },
    otherColor: {
      main: grey[500],
    },
  },
})
