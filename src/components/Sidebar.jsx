import styled from '@emotion/styled'
import {
  Drafts,
  Group,
  Home,
  Inbox,
  ModeNight,
  Pages,
  People,
  PeopleOutlined,
  Person,
  Settings,
  Store,
} from '@mui/icons-material'
import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Switch,
} from '@mui/material'
import React from 'react'

const Sidebar = () => {
  return (
    <Box
      // bgcolor='white'
      flex={1}
      p={2}
      sx={{ display: { xs: 'none', sm: 'block' } }}
    >
      <Box position={'fixed'}>
        <List>
          <ListItem disablePadding>
            <ListItemButton component='a' href='#home'>
              <ListItemIcon>
                <Home />
              </ListItemIcon>
              <ListItemText
                primary='Home'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <Pages />
              </ListItemIcon>
              <ListItemText
                primary='Pages'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <Group />
              </ListItemIcon>
              <ListItemText
                primary='Groups'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <Store />
              </ListItemIcon>
              <ListItemText
                primary='Marketplace'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <PeopleOutlined />
              </ListItemIcon>
              <ListItemText
                primary='Friends'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <Settings />
              </ListItemIcon>
              <ListItemText
                primary='Settings'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <Person />
              </ListItemIcon>
              <ListItemText
                primary='Profile'
                sx={{ display: { sm: 'none', md: 'block' } }}
              />
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <ModeNight />
              </ListItemIcon>
              <Box sx={{ display: { sm: 'none', md: 'block' } }}>
                <Switch />
              </Box>
            </ListItemButton>
          </ListItem>
        </List>
      </Box>
    </Box>
  )
}

export default Sidebar
